
CREATE DATABASE Flux;

- DROP DATABASE Flux;


CREATE TABLE Game (
  GameID integer(4), #pKey
  GameType nvarchar(510),
  GridSize integer(4),
  Score integer(4)
);

CREATE TABLE GameTarget (
  GameTargetID integer (4), #pKey
  GameID integer(4),
  TargetID integer(4)
);

CREATE TABLE Player (
  TableID integer(4), #pKey
  PLayer nvarchar(510),
  StartPositionX integer(4),
  StartPositionY integer(4),
  Model integer
);

CREATE TABLE Step (
  StepID integer(4), #pKey
  GameID integer(4),
  PLayerID integer(4),
  StepNum integer(4),
  X integer(4),
  Y integer(4)
);

CREATE TABLE Target (
  TargetID integer(4), #pKey
  X integer(4),
  Y integer(4)
);

CREATE TABLE f_5205B521B7304558B76E5AE2901EFCEB_Data (
  _Data integer(4), #pKey
  FileData blob,
  FileFlags integer(4),
  FileName nvarchar(510),
  FileTimeStamp datetime(8),
  FileType nvarchar(510),
  FileURL text,
  MSysResources_Data integer(4)
);